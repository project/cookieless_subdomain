<?php

/**
 * @file
 * Admin page callbacks for the cookiless subdomain module.
 */

/**
 * Page generation function for admin/settings/cookieless_subdomain
 */
function cookieless_subdomain_admin_page() {
  $output = '';
  return $output . drupal_get_form('cookieless_subdomain_admin_settings_form');
}

/**
 * Form builder; Configure advagg settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function cookieless_subdomain_admin_settings_form() {
  $form = array();

  $form['cookieless_subdomains'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Cookieless subdomains'),
    '#default_value'  => implode("\n", variable_get('cookieless_subdomains', array())),
    '#description'    => t('Specify a list of subdomains, one per line. Make sure that all the speficied domains can serve the files that get stored in the files directory under the same path as the main domain.'),
  );

  return system_settings_form($form);
}

/**
 * Validate form values. Used to transform variables before they get saved.
 */
function cookieless_subdomain_admin_settings_form_validate($form, &$form_state) {

  $values = explode("\n", str_replace("\r", '', (trim($form_state['values']['cookieless_subdomains']))));

  $form_state['values']['cookieless_subdomains'] = $values;
}

